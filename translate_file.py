import xml.etree.ElementTree as ET
import lark
import math
import graph_io
import os
import matplotlib.pyplot as plt
import numpy as np
from graph import *


class FTG:
    def __init__(self, tle, graph, gates, bes):
        self.tle = tle
        self.graph = graph
        self.gates = gates
        self.bes = bes
        self.directed_graph = None

def read_file(name):
    with open(name, 'r') as f:
        d = f.read()
    return d

def remove_if_qt(string):
    if len(string) > 2 and string[0] == '"' and string[-1] == '"':
        return string[1:-1]
    return string

def add_if_not_qt(string):
    if len(string) > 2 and string[0] == '"' and string[-1] == '"':
        return string
    else:
        return f'"{string}"'

def prob_to_failrate(time, prob):
    return -math.log(1 - prob) / time

def find_root(pred):
    rval = list(pred.keys())[0]
    while rval in pred:
        rval = pred[rval]
    return rval

def parse_xml_sft(filename, time):
    tree = ET.parse(filename)
    root = tree.getroot()

    for l in root.findall('.//label/..'):
        l.remove(l.find('./label'))

    events = root.findall('.//define-gate')
    probs = root.findall('.//define-basic-event')

    succ = dict()
    types = dict()
    for e in events:
        name = e.get('name')
        children = [c.get('name') for c in e[0]]
        type = e[0].tag
        if type not in ['and', 'or', 'atleast', 'gate', 'basic-event']:
            print(type)
            raise ValueError
        if type in ['gate', 'basic-event']:
            children = [e[0].get('name')]
            type = 'and'
        if type == 'atleast':
            n = e[0].get('min')
            type = ('vot', n)
        succ[name] = children
        types[name] = type

    pred = dict()
    for i, j in succ.items():
        for k in j:
            pred[k] = i
    toplevel = find_root(pred)

    bes = dict()
    for b in probs:
        name = b.get('name')
        if b[0].tag == 'float':
            value = float(b[0].get('value'))
            value = prob_to_failrate(time, value)
        elif b[0].tag == 'exponential':
            value = float(b[0][0].get('value'))
        bes[name] = value

    return FTG(toplevel, succ, types, bes)

lark_parser = lark.Lark(r'''
start: (line | _NEWLINE)+
line: (toplevel | gate | be) _EOL
toplevel: "toplevel" NAME
gate: NAME _type children
children: NAME+
_type: SIMPLEBOOL | vot | DYNAMICTYPES
vot: INT "of" INT
be: NAME assignment+
assignment: ATTR "=" NUMBER

ATTR: "lambda" | "prob" | "dorm" | "res"
SIMPLEBOOL: "and" | "or"
DYNAMICTYPES: "csp" | "wsp" | "hsp" | "pand" | "por" | "seq" | "mutex" | "fdep" | "pdep" | "vot3" | "vot7" | "vot2"

NAME: ESCAPED_STRING | /[a-zA-Z0-9_\-]+/

%import common.INT
%import common.NUMBER
%import common.ESCAPED_STRING
COMMENT: "//" /[^\n]*/ _NEWLINE
_NEWLINE: "\n"
_EOL: ";\n"
%ignore COMMENT
%ignore " "
''', parser="lalr")

class GetGraph(lark.Visitor):
    def __init__(self):
        super(GetGraph,self).__init__()
        self.graph = dict()
        self.gates = dict()
        self.bes = dict()

    def gate(self,tree):
        children = [add_if_not_qt(str(i)) for i in tree.children[2].children]
        typ = str(tree.children[1])
        if isinstance(tree.children[1], lark.Tree) and tree.children[1].data == 'vot':
            typ = ('vot', int(tree.children[1].children[0].value))
        name = add_if_not_qt(str(tree.children[0]))
        self.graph[name] = children
        self.gates[name] = typ

    def be(self,tree):
        name = add_if_not_qt(str(tree.children[0]))
        l = float(list(tree.find_pred(lambda x: x.children[0] == 'lambda'))[0].children[1])
        self.bes[name] = l

    def toplevel(self,tree):
        self.tle = add_if_not_qt(str(tree.children[0]))

def parse_dft_sft(filename):
    dft_str = read_file(filename)
    res = lark_parser.parse(dft_str)

    f = GetGraph()
    f.visit(res)

    return f

def parse_dft_file(filename, time=1.0):
    """
    Parses a .dft file and returns an FTG object.
    
    Parameters:
    - filename: str, the path to the .dft file.
    - time: float, the mission time for failrate to probability conversion (default is 1.0).
    
    Returns:
    - FTG object.
    """
    f = parse_dft_sft(filename)
    ftg = FTG(f.tle, f.graph, f.gates, f.bes)
    graph = Graph(True)
    vertices = dict()
    # Create vertex objects for basic events and gates and assign them to graph object.
    for i in ftg.bes.keys():
        v = i.replace('"', '')
        vertex = Vertex(graph, v)
        graph.add_vertex(vertex)
        vertices[v] = vertex
    for i in ftg.gates.keys():
        v = i.replace('"', '')
        vertex = Vertex(graph, v)
        graph.add_vertex(vertex)
        vertices[v] = vertex
    
    # Create edge objects for basic events and gates and assign them to graph object.
    for i,tails in ftg.graph.items():
        head = i.replace('"', '')
        if i == f.tle:
            graph.root = vertices[head]
        if i in ftg.gates:
            if ftg.gates[i] == 'fdep':
                tail = tails[0].replace('"', '')
                graph += Edge(vertices[tail], vertices[head])
                for j in range(1, len(tails)):
                    tail = tails[j].replace('"', '')
                    # For fdep we reverse head and tail
                    graph += Edge(vertices[head], vertices[tail])
                continue
            if ftg.gates[i] == 'seq':
                last_vertex = None
                for j in range(len(tails)):
                    first = tails[j].replace('"', '')
                    if j == len(tails) - 1:
                        last_vertex = first
                        break
                    second = tails[j + 1].replace('"','')
                    graph += Edge(vertices[first], vertices[second])
                graph += Edge(vertices[last_vertex], vertices[head])
        for j in tails:
            if j in ftg.gates:
                if ftg.gates[j] == 'fdep':
                    continue
            tail = j.replace('"', '')
            graph += Edge(vertices[tail], vertices[head])
    ftg.directed_graph = graph
    
    return ftg

def in_degree_entropy(graph : Graph):
    num_edges = len(graph.edges)
    entropy = 0
    for v in graph.vertices:
        degree_fraction = v.in_degree / num_edges
        if degree_fraction > 0: 
            entropy -= degree_fraction * math.log(degree_fraction, 2)
    return entropy


def out_degree_entropy(graph : Graph):
    num_edges = len(graph.edges)
    entropy = 0
    for v in graph.vertices:
        degree_fraction = v.out_degree / num_edges
        if degree_fraction > 0: 
            entropy -= degree_fraction * math.log(degree_fraction, 2)

    return entropy

def in_degree_distribution_entropy(graph : Graph):
    N = len(graph.vertices)
    in_degrees = dict()
    for v in graph.vertices:
        degree = v.in_degree
        if degree not in in_degrees:
            in_degrees[degree] = 0
        in_degrees[degree] += 1
    I = 0
    for n in in_degrees.keys():
        fraction = in_degrees[n] / N
        I -= fraction * math.log(fraction, 2)
    return I

def out_degree_distribution_entropy(graph : Graph):
    N = len(graph.vertices)
    out_degrees = dict()
    for v in graph.vertices:
        degree = v.out_degree
        if degree not in out_degrees:
            out_degrees[degree] = 0
        out_degrees[degree] += 1
    I = 0
    for n in out_degrees.keys():
        fraction = out_degrees[n] / N
        I -= fraction * math.log(fraction, 2)
    return I

def list_visible_files(directory):
    visible_files = []
    for file in os.listdir(directory):
        dir, filename = os.path.split(file)
        if not filename.startswith('.'):
            visible_files.append(file)
    return visible_files

def calculate_folder_random(directory_path):
    in_entropies = dict()
    out_entropies = dict()
    std_dev_in = dict()
    std_dev_out = dict()
    for folder in os.listdir(directory_path):
        print(folder)
        in_entrop_list = []
        out_entrop_list = []
        folder_path = os.path.join(directory_path, folder)
        min_size = 0
        for filename in os.listdir(folder_path):
            file_path = os.path.join(folder_path, filename)
            with open(file_path) as f:
                graphs = graph_io.read_graph(Graph, f)
            ft = graphs[0]
            # with open('random_big_graphs.dot', 'w') as f:
            #     graph_io.write_dot(ftg.directed_graph, f, True)
            in_entropy = in_degree_entropy(ft)
            out_entropy = out_degree_entropy(ft)
            in_entrop_list.append(in_entropy)
            out_entrop_list.append(out_entropy)
            size = len(ft.vertices)
            if size < min_size or min_size == 0:
                min_size = size 
        avg_in_entropy = sum(in_entrop_list) / len(in_entrop_list)
        avg_out_entropy = sum(out_entrop_list) / len(out_entrop_list)

        in_entropies[min_size] = avg_in_entropy
        out_entropies[min_size] = avg_out_entropy
        
        std_dev_in[min_size] = np.std(in_entrop_list, ddof = 1)
        std_dev_out[min_size] = np.std(out_entrop_list, ddof = 1) 

    
    sorted_keys = sorted(in_entropies.keys())
    sorted_values_in = [in_entropies[key] for key in sorted_keys]
    sorted_values_out = [out_entropies[key] for key in sorted_keys]
    std_in = [std_dev_in[key] for key in sorted_keys]
    std_out = [std_dev_out[key] for key in sorted_keys]

    upper_in = [sorted_values_in[i] + std_in[i] for i in range(len(sorted_values_in))]
    lower_in = [sorted_values_in[i] - std_in[i] for i in range(len(sorted_values_in))]
    upper_out = [sorted_values_out[i] + std_out[i] for i in range(len(sorted_values_out))]
    lower_out = [sorted_values_out[i] - std_out[i] for i in range(len(sorted_values_out))]
    
    plt.subplot(211)
    plt.plot(sorted_keys, sorted_values_in, label = 'Entropy')
    plt.plot(sorted_keys, upper_in, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_in, 'r--', label='Lower Std Dev', linewidth=1)
    plt.legend()
    plt.ylabel('In-Degree Entropy')  # Label for the Y-axis
    #plt.xlabel('Graph size')  # Label for the Y-axis
    plt.subplot(212)
    plt.plot(sorted_keys, sorted_values_out, label='Entropy')
    plt.plot(sorted_keys, upper_out, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_out, 'r--', label='Lower Std Dev', linewidth=1)
    plt.ylabel('Out-Degree Entropy')  # Label for the Y-axis
    plt.xlabel('Graph order |V(G)|')  # Label for the Y-axis
    plt.legend()
    plt.savefig('all-random_degree_entropy_order.png')

def distribution_entropy_random(directory_path):
    in_entropies = dict()
    out_entropies = dict()
    std_dev_in = dict()
    std_dev_out = dict()
    for folder in os.listdir(directory_path):
        print(folder)
        in_entrop_list = []
        out_entrop_list = []
        folder_path = os.path.join(directory_path, folder)
        min_size = 0
        for filename in list_visible_files(folder_path):
            file_path = os.path.join(folder_path, filename)
            with open(file_path, 'r') as f:
                graphs = graph_io.read_graph(Graph, f)
            ftg = graphs[0]
            in_entropy = in_degree_distribution_entropy(ftg)
            out_entropy = out_degree_distribution_entropy(ftg)
            in_entrop_list.append(in_entropy)
            out_entrop_list.append(out_entropy)
            size = len(ftg)
            if size < min_size or min_size == 0:
                min_size = size 
        avg_in_entropy = sum(in_entrop_list) / len(in_entrop_list)
        avg_out_entropy = sum(out_entrop_list) / len(out_entrop_list)

        in_entropies[min_size] = avg_in_entropy
        out_entropies[min_size] = avg_out_entropy
        
        std_dev_in[min_size] = np.std(in_entrop_list, ddof = 0)
        std_dev_out[min_size] = np.std(out_entrop_list, ddof = 0)

    
    sorted_keys = sorted(in_entropies.keys())
    sorted_values_in = [in_entropies[key] for key in sorted_keys]
    sorted_values_out = [out_entropies[key] for key in sorted_keys]
    std_in = [std_dev_in[key] for key in sorted_keys]
    std_out = [std_dev_out[key] for key in sorted_keys]

    upper_in = [sorted_values_in[i] + std_in[i] for i in range(len(sorted_values_in))]
    lower_in = [sorted_values_in[i] - std_in[i] for i in range(len(sorted_values_in))]
    upper_out = [sorted_values_out[i] + std_out[i] for i in range(len(sorted_values_out))]
    lower_out = [sorted_values_out[i] - std_out[i] for i in range(len(sorted_values_out))]
    
    plt.subplot(211)
    plt.plot(sorted_keys, sorted_values_in, label = 'In-Degree entropy')
    plt.plot(sorted_keys, upper_in, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_in, 'r--', label='Lower Std Dev', linewidth=1)
    plt.legend()
    plt.ylabel('In Entropy Values')  # Label for the Y-axis
    #plt.xlabel('Graph size')  # Label for the Y-axis
    plt.subplot(212)
    plt.plot(sorted_keys, sorted_values_out, label='Out-Degree entropy')
    plt.plot(sorted_keys, upper_out, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_out, 'r--', label='Lower Std Dev', linewidth=1)
    plt.ylabel('Out Entropy Values')  # Label for the Y-axis
    plt.xlabel('Graph size')  # Label for the Y-axis
    plt.legend()
    plt.savefig('relation_remake_random_size.png')

def distribution_entropy(directory_path):
    in_entropies = dict()
    out_entropies = dict()
    std_dev_in = dict()
    std_dev_out = dict()
    for folder in list_visible_files(directory_path):
        print(folder)
        in_entrop_list = []
        out_entrop_list = []
        folder_path = os.path.join(directory_path, folder)
        min_size = 0
        for filename in list_visible_files(folder_path):
            file_path = os.path.join(folder_path, filename)
            ftg = parse_dft_file(file_path)
            # with open('random_big_graphs.dot', 'w') as f:
            #     graph_io.write_dot(ftg.directed_graph, f, True)
            in_entropy = in_degree_distribution_entropy(ftg.directed_graph)
            out_entropy = out_degree_distribution_entropy(ftg.directed_graph)
            in_entrop_list.append(in_entropy)
            out_entrop_list.append(out_entropy)
            size = len(ftg.directed_graph.vertices)
            if size < min_size or min_size == 0:
                min_size = size 
        avg_in_entropy = sum(in_entrop_list) / len(in_entrop_list)
        avg_out_entropy = sum(out_entrop_list) / len(out_entrop_list)

        in_entropies[min_size] = avg_in_entropy
        out_entropies[min_size] = avg_out_entropy
        
        std_dev_in[min_size] = np.std(in_entrop_list, ddof = 0)
        std_dev_out[min_size] = np.std(out_entrop_list, ddof = 0)

    
    sorted_keys = sorted(in_entropies.keys())
    sorted_values_in = [in_entropies[key] for key in sorted_keys]
    sorted_values_out = [out_entropies[key] for key in sorted_keys]
    std_in = [std_dev_in[key] for key in sorted_keys]
    std_out = [std_dev_out[key] for key in sorted_keys]

    upper_in = [sorted_values_in[i] + std_in[i] for i in range(len(sorted_values_in))]
    lower_in = [sorted_values_in[i] - std_in[i] for i in range(len(sorted_values_in))]
    upper_out = [sorted_values_out[i] + std_out[i] for i in range(len(sorted_values_out))]
    lower_out = [sorted_values_out[i] - std_out[i] for i in range(len(sorted_values_out))]
    
    plt.subplot(211)
    plt.plot(sorted_keys, sorted_values_in, label = 'In-Degree entropy')
    plt.plot(sorted_keys, upper_in, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_in, 'r--', label='Lower Std Dev', linewidth=1)
    plt.legend()
    plt.ylabel('In Entropy Values')  # Label for the Y-axis
    #plt.xlabel('Graph size')  # Label for the Y-axis
    plt.subplot(212)
    plt.plot(sorted_keys, sorted_values_out, label='Out-Degree entropy')
    plt.plot(sorted_keys, upper_out, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_out, 'r--', label='Lower Std Dev', linewidth=1)
    plt.ylabel('Out Entropy Values')  # Label for the Y-axis
    plt.xlabel('Graph size')  # Label for the Y-axis
    plt.legend()
    plt.savefig('relation_real_degree_distribution_entropy_order.png')

def calculate_folder(directory_path):
    in_entropies = dict()
    out_entropies = dict()
    std_dev_in = dict()
    std_dev_out = dict()
    for folder in os.listdir(directory_path):
        print(folder)
        in_entrop_list = []
        out_entrop_list = []
        folder_path = os.path.join(directory_path, folder)
        min_size = 0
        for filename in os.listdir(folder_path):
            file_path = os.path.join(folder_path, filename)
            ftg = parse_dft_file(file_path)
            # with open('random_big_graphs.dot', 'w') as f:
            #     graph_io.write_dot(ftg.directed_graph, f, True)
            in_entropy = in_degree_entropy(ftg.directed_graph)
            out_entropy = out_degree_entropy(ftg.directed_graph)
            in_entrop_list.append(in_entropy)
            out_entrop_list.append(out_entropy)
            size = len(ftg.directed_graph.vertices)
            if size < min_size or min_size == 0:
                min_size = size 
        avg_in_entropy = sum(in_entrop_list) / len(in_entrop_list)
        avg_out_entropy = sum(out_entrop_list) / len(out_entrop_list)

        in_entropies[min_size] = avg_in_entropy
        out_entropies[min_size] = avg_out_entropy
        
        std_dev_in[min_size] = np.std(in_entrop_list, ddof = 1)
        std_dev_out[min_size] = np.std(out_entrop_list, ddof = 1)

    
    sorted_keys = sorted(in_entropies.keys())
    sorted_values_in = [in_entropies[key] for key in sorted_keys]
    sorted_values_out = [out_entropies[key] for key in sorted_keys]
    std_in = [std_dev_in[key] for key in sorted_keys]
    std_out = [std_dev_out[key] for key in sorted_keys]

    upper_in = [sorted_values_in[i] + std_in[i] for i in range(len(sorted_values_in))]
    lower_in = [sorted_values_in[i] - std_in[i] for i in range(len(sorted_values_in))]
    upper_out = [sorted_values_out[i] + std_out[i] for i in range(len(sorted_values_out))]
    lower_out = [sorted_values_out[i] - std_out[i] for i in range(len(sorted_values_out))]
    
    plt.subplot(211)
    plt.plot(sorted_keys, sorted_values_in, label = 'Entropy')
    plt.plot(sorted_keys, upper_in, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_in, 'r--', label='Lower Std Dev', linewidth=1)
    plt.legend()
    plt.ylabel('In-Degree Entropy')  # Label for the Y-axis
    #plt.xlabel('Graph size')  # Label for the Y-axis
    plt.subplot(212)
    plt.plot(sorted_keys, sorted_values_out, label='Entropy')
    plt.plot(sorted_keys, upper_out, 'r--', label='Upper Std Dev', linewidth=1)
    plt.plot(sorted_keys, lower_out, 'r--', label='Lower Std Dev', linewidth=1)
    plt.ylabel('Out-Degree Entropy')  # Label for the Y-axis
    plt.xlabel('Graph order |V(G)|')  # Label for the Y-axis
    plt.legend()
    plt.savefig('allreal_order_entropy.png')

def calculate_files_entropy(directory_path):
    in_entropies = dict()
    out_entropies = dict()
    for filename in list_visible_files(directory_path):
        print(filename)
        file_path = os.path.join(directory_path, filename)
        ftg = parse_dft_file(file_path)
        # with open('random_big_graphs.dot', 'w') as f:
        #     graph_io.write_dot(ftg.directed_graph, f, True)
        in_entropy = in_degree_entropy(ftg.directed_graph)
        out_entropy = out_degree_entropy(ftg.directed_graph)
        #size = len(ftg.directed_graph.vertices)
        size = len(ftg.directed_graph.edges)
        if size not in in_entropies:
            out_entropies[size] = [out_entropy]
            in_entropies[size] = [in_entropy]
        else:
            out_entropies[size].append(out_entropy)
            in_entropies[size].append(in_entropy)
    plot_x_in = dict()
    plot_x_out = dict()
    std_dev_in = dict()
    std_dev_out = dict()    
    for size in in_entropies.keys():
        if len(in_entropies[size]) == 1:
            plot_x_in[size] = in_entropies[size][0]
            plot_x_out[size] = out_entropies[size][0]
            std_dev_in[size] = 0
            std_dev_out[size] = 0
            continue
        sum_in = sum(in_entropies[size])
        sum_out = sum(out_entropies[size])
        plot_x_in[size] = sum_in/len(in_entropies[size])
        plot_x_out[size] = sum_out/len(out_entropies[size])
        std_dev_in[size] = np.std(in_entropies[size], ddof = 0)
        std_dev_out[size] = np.std(out_entropies[size], ddof = 0)
    sorted_keys = sorted(plot_x_in.keys())
    sorted_values_in = [plot_x_in[key] for key in sorted_keys]
    sorted_values_out = [plot_x_out[key] for key in sorted_keys]
    std_in = [std_dev_in[key] for key in sorted_keys]
    std_out = [std_dev_out[key] for key in sorted_keys]

    plt.subplot(211)
    plt.errorbar(sorted_keys, sorted_values_in, yerr=std_in, label = 'In-degree entropy')
    plt.ylabel('In-degree entropy')  # Label for the Y-axis
    plt.legend()
    #plt.xlabel('Graph size')  # Label for the Y-axis
    plt.subplot(212)
    plt.errorbar(sorted_keys, sorted_values_out, yerr=std_out, label='Out-degree entropy')
    plt.ylabel('Out-degree entropy')  # Label for the Y-axis
    plt.xlabel('Graph size |E(G)|')  # Label for the Y-axis
    plt.legend()
    plt.savefig('real200_degree_entropy-size.png')
    plt.clf()

    plt.figure()
    plt.loglog(sorted_keys, sorted_values_in, label='In-Degree Entropy')
    plt.loglog(sorted_keys, sorted_values_out, label='Out-Degree Entropy')
    plt.xlabel('Log(Graph Size)')
    plt.ylabel('Log(Entropy)')
    plt.legend()
    plt.title('Log-Log Plot of Entropy vs. Graph Order/Size')
    plt.savefig('loglogreal200_degree_entropy-order.png')
        
    

def calculate_graph_files_entropy(directory_path):
    in_entropies = dict()
    out_entropies = dict()
    size = 0
    for filename in os.listdir(directory_path):
        file_path = os.path.join(directory_path, filename)
        with open(file_path) as f:
            graphs = graph_io.read_graph(Graph, f)
        graph = graphs[0]
        in_degree_sequence = []
        out_degree_sequence = []
        degree_sequence = []
        for v in graph.vertices:
            in_degree_sequence.append(v.in_degree)
            out_degree_sequence.append(v.out_degree)
            degree_sequence.append(v.degree)
        # with open('random_big_graphs.dot', 'w') as f:
        #     graph_io.write_dot(ftg.directed_graph, f, True)
        in_entropy = in_degree_entropy(graph)
        out_entropy = out_degree_entropy(graph)
        size = len(graph.edges)
        if size not in in_entropies:
            out_entropies[size] = [out_entropy]
            in_entropies[size] = [in_entropy]
        else:
            out_entropies[size].append(out_entropy)
            in_entropies[size].append(in_entropy)
    plot_x_in = dict()
    plot_x_out = dict()
    std_dev_in = dict()
    std_dev_out = dict()     
    for size in in_entropies.keys():
        if len(in_entropies[size]) == 1:
            plot_x_in[size] = in_entropies[size][0]
            plot_x_out[size] = out_entropies[size][0]
            std_dev_in[size] = 0
            std_dev_out[size] = 0
            continue
        sum_in = sum(in_entropies[size])
        sum_out = sum(out_entropies[size])
        std_dev_in[size] = np.std(in_entropies[size], ddof = 0)
        std_dev_out[size] = np.std(out_entropies[size], ddof = 0)
        plot_x_in[size] = sum_in/len(in_entropies[size])
        plot_x_out[size] = sum_out/len(out_entropies[size])
    
    sorted_keys = sorted(plot_x_in.keys())
    sorted_values_in = [plot_x_in[key] for key in sorted_keys]
    sorted_values_out = [plot_x_out[key] for key in sorted_keys]
    std_in = [std_dev_in[key] for key in sorted_keys]
    std_out = [std_dev_out[key] for key in sorted_keys]

    plt.subplot(211)
    plt.errorbar(sorted_keys, sorted_values_in, yerr=std_in, label = 'In-degree entropy')
    plt.ylabel('In-degree entropy')  # Label for the Y-axis
    #plt.xlabel('Graph size')  # Label for the Y-axis
    plt.legend()
    plt.subplot(212)
    plt.errorbar(sorted_keys, sorted_values_out, yerr=std_out, label='Out-degree entropy')
    plt.ylabel('Out-degree entropy')  # Label for the Y-axis
    plt.xlabel('Graph size |E(G)|')  # Label for the Y-axis
    plt.legend()
    plt.savefig('allrandom_entropy-size.png')

    plt.figure()
    plt.loglog(sorted_keys, sorted_values_in, label='In-Degree Entropy')
    plt.loglog(sorted_keys, sorted_values_out, label='Out-Degree Entropy')
    plt.xlabel('Log(Graph Size)')
    plt.ylabel('Log(Entropy)')
    plt.legend()
    plt.title('Log-Log Plot of Entropy vs. Graph Order/Size')
    plt.savefig('loglogallrandom_degree_entropy-order.png')



def degree_distribution(graph : Graph, figure_name, file_name, random = False):
    in_degree_sequence = []
    out_degree_sequence = []
    degree_sequence = []
    for v in graph.vertices:
        in_degree_sequence.append(v.in_degree)
        out_degree_sequence.append(v.out_degree)
        degree_sequence.append(v.degree)
    
    if not random:
        with open('dot_files_real_trees/' + file_name + '.dot', 'w') as f:
            graph_io.write_dot(graph, f, True)
    else:
        with open('dot_files_random_trees/' + file_name + '.dot', 'w') as f:
            graph_io.write_dot(graph, f, True)

    #in_entropy = in_degree_entropy(graph)
    #out_entropy = out_degree_entropy(graph)
    in_distribution_entropy = in_degree_distribution_entropy(graph)
    out_distribution_entropy = out_degree_distribution_entropy(graph)


    plt.figure(figsize=(15, 9))
    plt.subplot(121)
    plt.hist(in_degree_sequence, bins = range(min(in_degree_sequence), max(in_degree_sequence) + 2), align = "left", rwidth=0.8)
    plt.xticks(range(min(in_degree_sequence), max(in_degree_sequence) + 1))
    plt.xlabel("In-Degrees") #| InEntropy: " + "{:.2f}".format(in_entropy))
    plt.ylabel("Number of vertices")
    plt.subplot(122)
    plt.hist(out_degree_sequence, bins = range(min(out_degree_sequence), max(out_degree_sequence) + 2), align = "left", rwidth=0.8)
    plt.xticks(range(min(out_degree_sequence), max(out_degree_sequence) + 1))
    plt.xlabel("Out-Degrees") #Distribution | OutEntropy: " + "{:.2f}".format(out_entropy) + ' | Graph Size: ' + str(len(graph.vertices)))
    plt.ylabel("Number of vertices")
    plt.savefig(figure_name)
    plt.clf()
    plt.close()
    return in_distribution_entropy, out_distribution_entropy, in_degree_sequence, out_degree_sequence

def degree_distribution_file(file_path, figure_name, file_name, random = False):
    g = 0
    #print('File : ' + file_name)
    if not random:
        print('File : ' + file_name)
    if not random:
        ftg = parse_dft_file(file_path)
        g = ftg.directed_graph
    else:
        with open(file_path) as f:
            graphs = graph_io.load_graph(f, Graph)
        g = graphs
    max_in = 0
    max_out = 0
    for v in g.vertices:
        if v.in_degree > max_in:
            max_in = v.in_degree
        if v.out_degree > max_out:
            max_out = v.out_degree
    print("Vertices : " + str(len(g.vertices)))
    print("Edges : " + str(len(g.edges)))
    #print('root child: ' + str(len(g.root.head_edges)))
    #print('max child: ' + str(max_in))
    #print('max out: ' + str(max_out))
    if type(g) is int:
        print('Error')
        return
    if not random:
        in_d_entropy, out_d_entropy, in_s, out_s = degree_distribution(g, figure_name, file_name, random = False)
    else:
        in_d_entropy, out_d_entropy, in_s, out_s = degree_distribution(g, figure_name, file_name, random = True)
    return in_d_entropy, out_d_entropy, len(g.edges), in_s, out_s
    

def degree_distibution_folder(directory_path, random = False):
    in_d_entropies = dict()
    out_d_entropies = dict()
    in_seq_file = dict()
    out_seq_file = dict()
    for filename in list_visible_files(directory_path):
        file_path = os.path.join(directory_path, filename)
        dist_folder = 'distributions'
        random_folder = 'random_distributions'
        filename = filename.replace('.','')
        figure_path = os.path.join(dist_folder, filename)
        random_path = os.path.join(random_folder, filename)
        figure_name = figure_path + '_distribution'
        random_name = random_path + '_distribution'
        if not random:
            in_d_entropy, out_d_entropy, size, in_degree_seq, out_degree_seq = degree_distribution_file(file_path, figure_name, filename)
        else:
            in_d_entropy, out_d_entropy, size, in_degree_seq, out_degree_seq = degree_distribution_file(file_path, random_name, filename, random = True)
        in_seq_file[filename] = in_degree_seq
        out_seq_file[filename] = out_degree_seq
        if size not in in_d_entropies:
            in_d_entropies[size] = [in_d_entropy]
            out_d_entropies[size] = [out_d_entropy]
            continue
        in_d_entropies[size].append(in_d_entropy)
        out_d_entropies[size].append(out_d_entropy)

    #Plot multiple distributions in one figure
    # plt.figure()
    # plt.xlabel("In-Degrees") #| InEntropy: " + "{:.2f}".format(in_entropy))
    # plt.ylabel("Number of vertices")
    # color = ['blue', 'red', 'green', 'yellow']
    # all_bins = np.array([])
    # i = 0
    # for k in in_seq_file:
    #      #print(out_degree_sequence)
    #     if i == 4:
    #         break
    #     c = color[i]
    #     in_degree_sequence = in_seq_file[k]
    #     i += 1
    #     bins = range(min(in_degree_sequence), max(in_degree_sequence) + 2)
    #     all_bins = np.concatenate((all_bins, bins))
    #     plt.hist(in_degree_sequence, bins = bins, alpha = 0.5, align = "left", rwidth=0.8, color=c)
    #     plt.xticks(range(min(in_degree_sequence), max(in_degree_sequence) + 1))
    # plt.xticks(all_bins, rotation = 45)  # Optional: rotate labels for better readability
    # plt.savefig('2')
    # plt.clf()
    # plt.close()
    
    #Plot relation with entropy-size of graph
    plot_x_in = dict()
    plot_x_out = dict()
    std_dev_in = dict()
    std_dev_out = dict()     
    for size in in_d_entropies.keys():
        if len(in_d_entropies[size]) == 1:
            plot_x_in[size] = in_d_entropies[size][0]
            plot_x_out[size] = out_d_entropies[size][0]
            std_dev_in[size] = 0
            std_dev_out[size] = 0
            continue
        sum_in = sum(in_d_entropies[size])
        sum_out = sum(out_d_entropies[size])
        std_dev_in[size] = np.std(in_d_entropies[size], ddof = 0)
        std_dev_out[size] = np.std(out_d_entropies[size], ddof = 0)
        plot_x_in[size] = sum_in/len(in_d_entropies[size])
        plot_x_out[size] = sum_out/len(out_d_entropies[size])
    
    sorted_keys = sorted(plot_x_in.keys())
    sorted_values_in = [plot_x_in[key] for key in sorted_keys]
    sorted_values_out = [plot_x_out[key] for key in sorted_keys]
    std_in = [std_dev_in[key] for key in sorted_keys]
    std_out = [std_dev_out[key] for key in sorted_keys]
    plt.clf()
    plt.figure(figsize=(8, 6))
    plt.subplot(211)
    plt.errorbar(sorted_keys, sorted_values_in, yerr=std_in)
    plt.ylabel('In-degree distribution entropy')  # Label for the Y-axis
    #plt.xlabel('Graph size')  # Label for the Y-axis
    #plt.legend()
    plt.subplot(212)
    plt.errorbar(sorted_keys, sorted_values_out, yerr=std_out)
    plt.ylabel('Out-degree distribution entropy')  # Label for the Y-axis
    plt.xlabel('Graph size |G(E)|')  # Label for the Y-axis
    #plt.legend()
    plt.savefig('allreal_degree_distribution_entropy-size.png')
    plt.clf()

# Usage
def average_on_random(random_path):
    all_avg_in = dict()
    all_avg_out = dict()
    sizes = []
    for folder_name in os.listdir(random_path):
        size_folder_path = os.path.join(random_path, folder_name)
        avg_in, avg_out, size = calculate_graph_files_entropy(size_folder_path)
        all_avg_in[size] = (avg_in)
        all_avg_out[size] = (avg_out)
        sizes.append(size)
        print('Done with : ' + folder_name)

    sorted_keys = sorted(all_avg_out.keys())
    sorted_values_in = [all_avg_in[key] for key in sorted_keys]
    sorted_values_out = [all_avg_out[key] for key in sorted_keys]
    plt.subplot(211)
    plt.plot(sorted_keys, sorted_values_in, label = 'average out degree entropies')
    plt.ylabel('Entropy Values')  # Label for the Y-axis
    plt.xlabel('Graph size')  # Label for the Y-axis
    plt.subplot(212)
    plt.plot(sorted_keys, sorted_values_out , label='Out_degree entropies')
    plt.ylabel('Entropy Values')  # Label for the Y-axis
    plt.xlabel('Graph size')  # Label for the Y-axis
    plt.savefig('average_on_random_10-210.png')

# Calculating Distributions
folder_path_random = 'random'
folder_scrum_random = 'random_scram'
folder_path = 'real_ft_cat'
files_path = 'real_ft_cat/real_200'



#degree_distibution_folder(files_path, random = False)




