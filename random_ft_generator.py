from graph import *
import graph_io
import os
import random
import copy

BASIC_GATES = ['and', 'or']
ALL_GATES = ["csp" , "wsp" , "hsp" , "pand" , "por" , "seq" , "mutex" , "fdep" , "pdep"]

def init(vertices_nr : int):
    ft = Graph(True)
    root = Vertex(ft, 'root')
    ft.root = root
    ft.add_vertex(root)
    to_add_vertex = set()
    for i in range(0, vertices_nr - 1):
        vertex = Vertex(ft, i + 1)
        to_add_vertex.add(vertex)
        ft += vertex
    return ft, to_add_vertex

def shuffle_list(input_list):
    random.shuffle(input_list)
    return input_list

def generate_ft(vertices_nr : int, max_children : int, max_root_children : int, max_new_edges : int) -> Graph:
    ft, vertex_set = init(vertices_nr)
    levels = dict()
    levels[0] = [ft.root]
    current_level = 0
    status = True

    while status:
        status = False
        no_child_nodes = 0
        next_level = current_level + 1
        new_level = []
        for parent in levels[current_level]:
            rand_children = max_children + 1
            rand_root_children = max_root_children + 1
            if current_level == 0:
                children = random.randrange(2, rand_root_children)
            else:
                children = random.randrange(0 if no_child_nodes < (len(levels[current_level]) - 1) else 1, rand_children)
                if children == 0:
                    no_child_nodes += 1
                    continue
            current_len = len(vertex_set)
            range_nr = children
            if children > current_len:
                range_nr = current_len
            for i in range(range_nr):
                new_vertex = vertex_set.pop()
                new_edge = Edge(new_vertex, parent)
                new_level.append(new_vertex)
                ft += new_edge
        if len(new_level) == 0 and len(vertex_set) > 0:
            print('error')
        levels[next_level] = new_level
        if len(vertex_set) > 0:
            status = True
            current_level += 1

    
    
    # Randomly add multiple out edges for vertices on depth >= 2
    if len(levels.keys()) >= 2:
        for i in range(2, len(levels.keys())):
            previous_level = i - 1
            nodes_possible = levels[previous_level]
            current_vertices = levels[i]
            for vertex in current_vertices:
                probability_multiple_edges = random.random()
                if probability_multiple_edges > 0.1:
                    continue
                random_max = len(nodes_possible) - 1
                if random_max == 0:
                    continue
                nr_new_edges = random.randrange(0, max_new_edges + 1)
                head_neighbours = vertex.get_out_neighbours()
                shuffled = shuffle_list(nodes_possible)
                for node in shuffled:
                    if node not in head_neighbours:
                        new_edge = Edge(vertex, node)
                        ft += new_edge
                        nr_new_edges -= 1
                        if nr_new_edges <= 0:
                            break
    ft.levels = levels
    return ft



init_path = 'random'
max_len = 301
min_len = 300
size_set = 1000
max_children = 17
max_root_children = 59
max_new_edges = 13

for i in range(min_len, max_len):
    string = 'size_'
    folder_name = string + str(min_len)#+ str(i)
    folder_path = os.path.join(init_path, folder_name)
    os.makedirs(folder_path, exist_ok = True)
    for j in range(size_set):
        ft = generate_ft(i, max_children, max_root_children, max_new_edges)
        file_name = str(i) + "_" + str(j) + '.gr'
        file_path = os.path.join(folder_path, file_name)
        with open(file_path, 'w') as f:
            graph_io.save_graph(ft, f, True)
    print('Done with : ' + str(i))

    