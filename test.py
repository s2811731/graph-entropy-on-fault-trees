import math

def out_degree_entropy(sequence):
    m = sum(sequence)
    entropy = 0
    for degree in sequence:
        degree_fraction = degree / m
        if degree_fraction > 0: 
            entropy -= degree_fraction * math.log(degree_fraction, 2)
    return entropy

sequences = [[5], [4, 1], [3,2], [3,1,1], [2, 1, 1, 1], [1, 1, 1, 1, 1]]
entropies = dict()
for s in sequences:
    entropy = out_degree_entropy(s)
    entropies[entropy] = s

sorting = sorted(entropies.keys(), reverse=True)
for k in sorting:
    print('Seq = ' + str(entropies[k]) + '  Entropy = ' + str(k))

import matplotlib.pyplot as plt
import numpy as np

# Example data: degree values and the corresponding number of vertices
degree_values = [0, 1, 2, 3, 4, 5, 6, 7, 8]
num_vertices = [100, 50, 20, 0, 0, 10, 0, 0, 1]

# Calculate the total number of vertices
total_vertices = sum(num_vertices)

# Normalize the data to get the probability distribution
probability_distribution = [num / total_vertices for num in num_vertices]

# Plotting the probability distribution
plt.bar(degree_values, probability_distribution, color='g', alpha=0.6)

# Adding labels and title
plt.xlabel('In-Degrees')
plt.ylabel('Probability')
plt.title('Probability Distribution of In-Degrees')

# Display the plot
plt.show()

