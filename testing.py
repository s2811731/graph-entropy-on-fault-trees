import networkx as nx
import os
import pydot
import random

def generate_rooted_trees(n):
    # This dictionary will store the trees with different roots
    all_trees = []

    # Generate all non-isomorphic trees of size n
    for tree in nx.nonisomorphic_trees(n):
        # For each tree, try every node as a possible root
        nodes = tree.nodes()
        print(nodes)
        node = nodes[0]
        # Create a deep copy of the tree for each root designation
        rooted_tree = nx.freeze(tree)  # freezing the tree ensures it's hashable
        # Store the tree and its root
        all_trees.append((rooted_tree, node))
    return all_trees

def write_trees_to_dot_files(trees, directory="nou"):
    
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    for i, (tree, root) in enumerate(trees):
        # Convert the NetworkX graph to a PyDot object
        dot = nx.nx_pydot.to_pydot(tree)
        
        # Mark the root in the DOT file
        for node in dot.get_nodes():
            if node.get_name() == str(root):
                node.set_shape('doublecircle')
            else:
                node.set_shape('circle')
        
        # Write the DOT format to a file
        filename = f"{directory}/tree_{i}_root_{root}.dot"
        dot.write_raw(filename)
        print(f"DOT file written: {filename}")

# rooted_trees = generate_rooted_trees(30)
# write_trees_to_dot_files(rooted_trees)

#print(random.randrange(0,3))